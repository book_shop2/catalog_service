package postgres

import (
	"context"
	"database/sql"
	"errors"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"

	"catalog_service/genproto/catalog_service"
	"catalog_service/models"
	"catalog_service/pkg/helper"
	"catalog_service/storage"
)

type AuthorRepo struct {
	db *pgxpool.Pool
}

func NewAuthorRepo(db *pgxpool.Pool) storage.AuthorRepoI {
	return &AuthorRepo{
		db: db,
	}
}

func (c *AuthorRepo) Create(ctx context.Context, req *catalog_service.CreateAuthor) (resp *catalog_service.AuthorPrimaryKey, err error) {

	var id = uuid.New()

	query := `INSERT INTO "author" (
				id,
				name,
				updated_at
			) VALUES ( $1, $2, now() )
		`

	_, err = c.db.Exec(ctx,
		query,
		id.String(),
		req.Name,
	)

	if err != nil {
		return nil, err
	}

	return &catalog_service.AuthorPrimaryKey{Id: id.String()}, nil
}

func (c *AuthorRepo) GetByPKey(ctx context.Context, req *catalog_service.AuthorPrimaryKey) (resp *catalog_service.Author, err error) {

	query := `
		SELECT
			id,
			name,
			created_at,
			updated_at
		FROM "author"
		WHERE deleted_at IS NULL AND id = $1
	`

	var (
		id        sql.NullString
		name      sql.NullString
		createdAt sql.NullString
		updatedAt sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&name,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &catalog_service.Author{
		Id:        id.String,
		Name:      name.String,
		CreatedAt: createdAt.String,
		UpdatedAt: updatedAt.String,
	}

	return
}

func (c *AuthorRepo) GetAll(ctx context.Context, req *catalog_service.GetListAuthorRequest) (resp *catalog_service.GetListAuthorResponse, err error) {

	resp = &catalog_service.GetListAuthorResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		// filter = " WHERE TRUE"
		sort = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			name,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "author"
		WHERE deleted_at IS NULL
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	query += sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			id        sql.NullString
			name      sql.NullString
			createdAt sql.NullString
			updatedAt sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&name,
			&createdAt,
			&updatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.Authors = append(resp.Authors, &catalog_service.Author{
			Id:        id.String,
			Name:      name.String,
			CreatedAt: createdAt.String,
			UpdatedAt: updatedAt.String,
		})
	}

	return resp, nil
}

func (c *AuthorRepo) Update(ctx context.Context, req *catalog_service.UpdateAuthor) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
			    "author"
			SET
				name = :name,
				updated_at = now()
			WHERE
				id = :id`
	params = map[string]interface{}{
		"id":   req.GetId(),
		"name": req.GetName(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *AuthorRepo) UpdatePatch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error) {

	var (
		set   = " SET "
		ind   = 0
		query string
	)

	if len(req.Fields) == 0 {
		err = errors.New("no updates provided")
		return
	}

	req.Fields["id"] = req.Id

	for key := range req.Fields {
		set += fmt.Sprintf(" %s = :%s ", key, key)
		if ind != len(req.Fields)-1 {
			set += ", "
		}
		ind++
	}

	query = `
		UPDATE
			"author"
	` + set + ` , updated_at = now()
		WHERE
			id = :id
	`

	query, args := helper.ReplaceQueryParams(query, req.Fields)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), err
}

func (c *AuthorRepo) Delete(ctx context.Context, req *catalog_service.AuthorPrimaryKey) error {

	query := `UPDATE "author" SET deleted_at = now() WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return err
	}

	return nil
}
