package postgres

import (
	"context"
	"fmt"
	"log"

	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"

	"catalog_service/config"
	"catalog_service/storage"
)

type Store struct {
	db            *pgxpool.Pool
	author        storage.AuthorRepoI
	book          storage.BookRepoI
	book_category storage.BookCategoryRepoI
	category      storage.CategoryRepoI
}

type Pool struct {
	db *pgxpool.Pool
}

func NewPostgres(ctx context.Context, cfg config.Config) (storage.StorageI, error) {
	config, err := pgxpool.ParseConfig(fmt.Sprintf(
		"postgres://%s:%s@%s:%d/%s?sslmode=disable",
		cfg.PostgresUser,
		cfg.PostgresPassword,
		cfg.PostgresHost,
		cfg.PostgresPort,
		cfg.PostgresDatabase,
	))
	if err != nil {
		return nil, err
	}

	config.MaxConns = cfg.PostgresMaxConnections

	pool, err := pgxpool.ConnectConfig(ctx, config)
	if err != nil {
		return nil, err
	}

	return &Store{
		db: pool,
	}, err
}

func (s *Store) CloseDB() {
	s.db.Close()
}

func (l *Store) Log(ctx context.Context, level pgx.LogLevel, msg string, data map[string]interface{}) {
	args := make([]interface{}, 0, len(data)+2) // making space for arguments + level + msg
	args = append(args, level, msg)
	for k, v := range data {
		args = append(args, fmt.Sprintf("%s=%v", k, v))
	}
	log.Println(args...)
}

func (s *Store) Author() storage.AuthorRepoI {
	if s.author == nil {
		s.author = NewAuthorRepo(s.db)
	}

	return s.author
}

func (s *Store) Book() storage.BookRepoI {
	if s.book == nil {
		s.book = NewBookRepo(s.db)
	}

	return s.book
}

func (s *Store) BookCategory() storage.BookCategoryRepoI {
	if s.book_category == nil {
		s.book_category = NewBookCategoryRepo(s.db)
	}

	return s.book_category
}

func (s *Store) Category() storage.CategoryRepoI {
	if s.category == nil {
		s.category = NewCategoryRepo(s.db)
	}

	return s.category
}
