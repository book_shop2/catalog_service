package postgres

import (
	"context"
	"database/sql"
	"errors"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"

	"catalog_service/genproto/catalog_service"
	"catalog_service/models"
	"catalog_service/pkg/helper"
	"catalog_service/storage"
)

type BookRepo struct {
	db *pgxpool.Pool
}

func NewBookRepo(db *pgxpool.Pool) storage.BookRepoI {
	return &BookRepo{
		db: db,
	}
}

func (c *BookRepo) Create(ctx context.Context, req *catalog_service.CreateBook) (resp *catalog_service.BookPrimaryKey, err error) {

	var id = uuid.New()

	query := `INSERT INTO "book" (
				id,
				name,
				author_id,
				updated_at
			) VALUES ( $1, $2, $3, now() )
		`

	_, err = c.db.Exec(ctx,
		query,
		id.String(),
		req.Name,
		req.AuthorId,
	)

	if err != nil {
		return nil, err
	}

	return &catalog_service.BookPrimaryKey{Id: id.String()}, nil
}

func (c *BookRepo) GetByPKey(ctx context.Context, req *catalog_service.BookPrimaryKey) (resp *catalog_service.Book, err error) {

	query := `
		SELECT
			id,
			name,
			author_id,
			created_at,
			updated_at
		FROM "book"
		WHERE deleted_at IS NULL AND id = $1
	`

	var (
		id        sql.NullString
		name      sql.NullString
		author_id sql.NullString
		createdAt sql.NullString
		updatedAt sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&name,
		&author_id,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &catalog_service.Book{
		Id:        id.String,
		Name:      name.String,
		AuthorId:  author_id.String,
		CreatedAt: createdAt.String,
		UpdatedAt: updatedAt.String,
	}

	return
}

func (c *BookRepo) GetAll(ctx context.Context, req *catalog_service.GetListBookRequest) (resp *catalog_service.GetListBookResponse, err error) {

	resp = &catalog_service.GetListBookResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		// filter = " WHERE TRUE"
		sort = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			name,
			author_id,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "book"
		WHERE deleted_at IS NULL
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	query += sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			id        sql.NullString
			name      sql.NullString
			author_id sql.NullString
			createdAt sql.NullString
			updatedAt sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&name,
			&author_id,
			&createdAt,
			&updatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.Books = append(resp.Books, &catalog_service.Book{
			Id:        id.String,
			Name:      name.String,
			AuthorId:  author_id.String,
			CreatedAt: createdAt.String,
			UpdatedAt: updatedAt.String,
		})
	}

	return
}

func (c *BookRepo) Update(ctx context.Context, req *catalog_service.UpdateBook) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
			    "book"
			SET
				name = :name,
				author_id = :author_id,
				updated_at = now()
			WHERE
				id = :id`
	params = map[string]interface{}{
		"id":        req.GetId(),
		"name":      req.GetName(),
		"author_id": req.GetAuthorId(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *BookRepo) UpdatePatch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error) {

	var (
		set   = " SET "
		ind   = 0
		query string
	)

	if len(req.Fields) == 0 {
		err = errors.New("no updates provided")
		return
	}

	req.Fields["id"] = req.Id

	for key := range req.Fields {
		set += fmt.Sprintf(" %s = :%s ", key, key)
		if ind != len(req.Fields)-1 {
			set += ", "
		}
		ind++
	}

	query = `
		UPDATE
			"book"
	` + set + ` , updated_at = now()
		WHERE
			id = :id
	`

	query, args := helper.ReplaceQueryParams(query, req.Fields)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), err
}

func (c *BookRepo) Delete(ctx context.Context, req *catalog_service.BookPrimaryKey) error {

	query := `UPDATE "book" SET deleted_at = now() WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return err
	}

	return nil
}
