package postgres

import (
	"context"
	"database/sql"
	"errors"
	"fmt"

	"github.com/jackc/pgx/v4/pgxpool"

	"catalog_service/genproto/catalog_service"
	"catalog_service/models"
	"catalog_service/pkg/helper"
	"catalog_service/storage"
)

type BookCategoryRepo struct {
	db *pgxpool.Pool
}

func NewBookCategoryRepo(db *pgxpool.Pool) storage.BookCategoryRepoI {
	return &BookCategoryRepo{
		db: db,
	}
}

func (c *BookCategoryRepo) Create(ctx context.Context, req *catalog_service.CreateBookCategory) (resp *catalog_service.BookCategoryPrimaryKey, err error) {

	query := `INSERT INTO "book_category" (
				book_id,
				category_id,
				updated_at
			) VALUES ( $1, $2, now() )
		`

	_, err = c.db.Exec(ctx,
		query,
		req.BookId,
		req.CategoryId,
	)

	if err != nil {
		return nil, err
	}

	return &catalog_service.BookCategoryPrimaryKey{BookId: req.BookId}, nil
}

func (c *BookCategoryRepo) GetByPKey(ctx context.Context, req *catalog_service.BookCategoryPrimaryKey) (resp *catalog_service.BookCategoryList, err error) {

	var (
		bookId       sql.NullString
		bookTitle    sql.NullString
		authorName   sql.NullString
		categoryName sql.NullString
		parentName   sql.NullString
		createdAt    sql.NullString
		updatedAt    sql.NullString
	)

	query := `
		SELECT
			book.id,
		    book.name AS book_title,
			author.name AS author_name,
			book.created_at,
			book.updated_at
		FROM book_category
		JOIN book ON book.id = book_category.book_id
		JOIN author ON author.id = book.author_id
		WHERE book_category.book_id = $1
		GROUP BY book.id, author.name
	`

	err = c.db.QueryRow(ctx, query, req.BookId).Scan(
		&bookId,
		&bookTitle,
		&authorName,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		return nil, err
	}

	resp = &catalog_service.BookCategoryList{
		BookId:     bookId.String,
		BookTitle:  bookTitle.String,
		AuthorName: authorName.String,
		CreatedAt:  createdAt.String,
		UpdatedAt:  updatedAt.String,
	}

	queryChild := `
		SELECT
		    category.id,
		    category.name,
		    category_parent.name AS parent_name,
		    category.created_at,
		    category.updated_at
		FROM book_category
		JOIN category ON category.id = book_category.category_id
		JOIN category AS category_parent ON category_parent.id = category.parent_id
		WHERE book_category.book_id = $1
	`

	rows, err := c.db.Query(ctx, queryChild, resp.BookId)
	if err != nil {
		if err == sql.ErrNoRows {
			return resp, nil
		}

		return nil, err
	}

	for rows.Next() {

		err = rows.Scan(
			&bookId,
			&categoryName,
			&parentName,
			&createdAt,
			&updatedAt,
		)

		resp.BookCategories = append(resp.BookCategories, &catalog_service.CategoryLists{
			Id:         bookId.String,
			Name:       categoryName.String,
			ParentName: parentName.String,
			CreatedAt:  createdAt.String,
			UpdatedAt:  updatedAt.String,
		})
	}

	return
}

func (c *BookCategoryRepo) GetAll(ctx context.Context, req *catalog_service.GetListBookCategoryRequest) (resp *catalog_service.GetListBookCategoryResponse, err error) {

	resp = &catalog_service.GetListBookCategoryResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		// filter = " WHERE TRUE"
		sort = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			book.id,
    		book.name AS book_title,
			author.name AS author_name,
			book.created_at,
			book.updated_at
		FROM book_category
		JOIN book ON book.id = book_category.book_id
		JOIN author ON author.id = book.author_id
		GROUP BY book.id, author.name
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	query += sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	// get parent categories
	for rows.Next() {

		var (
			id         sql.NullString
			bookTitle  sql.NullString
			authorName sql.NullString
			createdAt  sql.NullString
			updatedAt  sql.NullString
		)

		err = rows.Scan(
			&resp.Count,
			&id,
			&bookTitle,
			&authorName,
			&createdAt,
			&updatedAt,
		)

		resp.Books = append(resp.Books, &catalog_service.BookCategoryList{
			BookId:     id.String,
			BookTitle:  bookTitle.String,
			AuthorName: authorName.String,
			CreatedAt:  createdAt.String,
			UpdatedAt:  updatedAt.String,
		})
	}

	// get category childs
	for _, category := range resp.Books {

		queryChild := `
			SELECT
				category.id,
				category.name,
				category_parent.name AS parent_name,
				category.created_at,
				category.updated_at
			FROM book_category
			JOIN category ON category.id = book_category.category_id
			JOIN category AS category_parent ON category_parent.id = category.parent_id
			WHERE book_category.book_id = $1
		`

		rows, err := c.db.Query(ctx, queryChild, category.BookId)
		if err != nil {
			if err == sql.ErrNoRows {
				return resp, nil
			}

			return nil, err
		}

		for rows.Next() {
			var (
				id           sql.NullString
				categoryName sql.NullString
				parentName   sql.NullString
				createdAt    sql.NullString
				updatedAt    sql.NullString
			)

			err = rows.Scan(
				&id,
				&categoryName,
				&parentName,
				&createdAt,
				&updatedAt,
			)

			category.BookCategories = append(category.BookCategories, &catalog_service.CategoryLists{
				Id:         id.String,
				Name:       categoryName.String,
				ParentName: parentName.String,
				CreatedAt:  createdAt.String,
				UpdatedAt:  updatedAt.String,
			})
		}
	}

	return
}

func (c *BookCategoryRepo) Update(ctx context.Context, req *catalog_service.UpdateBookCategory) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
			    "book_category"
			SET
				category_id = :category_id,
				updated_at = now()
			WHERE
			book_category.book_id = :book_id AND book_category.category_id = :check_category_id`
	params = map[string]interface{}{
		"book_id":           req.GetBookId(),
		"category_id":       req.GetToCategoryId(),
		"check_category_id": req.GetFromCategoryId(),
	}

	query, args := helper.ReplaceQueryParams(query, params)
	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *BookCategoryRepo) UpdatePatch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error) {

	var (
		set   = " SET "
		ind   = 0
		query string
	)

	if len(req.Fields) == 0 {
		err = errors.New("no updates provided")
		return
	}

	req.Fields["id"] = req.Id

	for key := range req.Fields {
		set += fmt.Sprintf(" %s = :%s ", key, key)
		if ind != len(req.Fields)-1 {
			set += ", "
		}
		ind++
	}

	query = `
		UPDATE
			"book_category"
	` + set + ` , updated_at = now()
		WHERE
			id = :id
	`

	query, args := helper.ReplaceQueryParams(query, req.Fields)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), err
}

func (c *BookCategoryRepo) Delete(ctx context.Context, req *catalog_service.DeleteBookCategory) error {

	query := `DELETE FROM "book_category" WHERE book_id = $1 AND category_id = $2`

	_, err := c.db.Exec(ctx, query, req.BookId, req.CategoryId)

	if err != nil {
		return err
	}

	return nil
}
