package postgres

import (
	"context"
	"database/sql"
	"errors"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"

	"catalog_service/genproto/catalog_service"
	"catalog_service/models"
	"catalog_service/pkg/helper"
	"catalog_service/storage"
)

type CategoryRepo struct {
	db *pgxpool.Pool
}

func NewCategoryRepo(db *pgxpool.Pool) storage.CategoryRepoI {
	return &CategoryRepo{
		db: db,
	}
}

func (c *CategoryRepo) Create(ctx context.Context, req *catalog_service.CreateCategory) (resp *catalog_service.CategoryPrimaryKey, err error) {

	var id = uuid.New()

	query := `INSERT INTO "category" (
				id,
				name,
				parent_id,
				updated_at
			) VALUES ( $1, $2, $3, now() )
		`

	_, err = c.db.Exec(ctx,
		query,
		id.String(),
		req.Name,
		helper.NewNullString(req.ParentId),
	)

	if err != nil {
		return nil, err
	}

	return &catalog_service.CategoryPrimaryKey{Id: id.String()}, nil
}

func (c *CategoryRepo) GetByPKey(ctx context.Context, req *catalog_service.CategoryPrimaryKey) (resp *catalog_service.CategoryList, err error) {

	var (
		id        sql.NullString
		name      sql.NullString
		parentID  sql.NullString
		createdAt sql.NullString
		updatedAt sql.NullString
	)

	query := `
		SELECT
			id,
			name,
			parent_id,
			created_at,
			updated_at
		FROM category
		WHERE id = $1 AND deleted_at IS NULL
	`

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&name,
		&parentID,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		return nil, err
	}

	resp = &catalog_service.CategoryList{
		Id:        id.String,
		Name:      name.String,
		ParentId:  parentID.String,
		CreatedAt: createdAt.String,
		UpdatedAt: updatedAt.String,
	}

	queryChild := `
		SELECT
			id,
			name,
			parent_id,
			created_at,
			updated_at
		FROM category
		WHERE parent_id = $1 AND deleted_at IS NULL
	`

	rows, err := c.db.Query(ctx, queryChild, resp.Id)
	if err != nil {
		if err == sql.ErrNoRows {
			return resp, nil
		}

		return nil, err
	}

	for rows.Next() {

		err = rows.Scan(
			&id,
			&name,
			&parentID,
			&createdAt,
			&updatedAt,
		)

		resp.Childs = append(resp.Childs, &catalog_service.Category{
			Id:        id.String,
			Name:      name.String,
			ParentId:  parentID.String,
			CreatedAt: createdAt.String,
			UpdatedAt: updatedAt.String,
		})
	}

	return
}

func (c *CategoryRepo) GetAll(ctx context.Context, req *catalog_service.GetListCategoryRequest) (resp *catalog_service.GetListCategoryResponse, err error) {

	resp = &catalog_service.GetListCategoryResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		// filter = " WHERE TRUE"
		sort = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			name,
			parent_id,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "category"
		WHERE parent_id IS NULL AND deleted_at IS NULL
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	query += sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	// get parent categories
	for rows.Next() {

		var (
			id        sql.NullString
			name      sql.NullString
			parentID  sql.NullString
			createdAt sql.NullString
			updatedAt sql.NullString
		)

		err = rows.Scan(
			&resp.Count,
			&id,
			&name,
			&parentID,
			&createdAt,
			&updatedAt,
		)

		resp.Categories = append(resp.Categories, &catalog_service.CategoryList{
			Id:        id.String,
			Name:      name.String,
			ParentId:  parentID.String,
			CreatedAt: createdAt.String,
			UpdatedAt: updatedAt.String,
		})
	}

	// get category childs
	for _, category := range resp.Categories {

		queryChild := `
			SELECT
				id,
				name,
				parent_id,
				created_at,
				updated_at
			FROM category
			WHERE parent_id = $1 AND deleted_at IS NULL
		`

		rows, err := c.db.Query(ctx, queryChild, category.Id)
		if err != nil {
			if err == sql.ErrNoRows {
				return resp, nil
			}

			return nil, err
		}

		for rows.Next() {
			var (
				id        sql.NullString
				name      sql.NullString
				parentID  sql.NullString
				createdAt sql.NullString
				updatedAt sql.NullString
			)

			err = rows.Scan(
				&id,
				&name,
				&parentID,
				&createdAt,
				&updatedAt,
			)

			category.Childs = append(category.Childs, &catalog_service.Category{
				Id:        id.String,
				Name:      name.String,
				ParentId:  parentID.String,
				CreatedAt: createdAt.String,
				UpdatedAt: updatedAt.String,
			})
		}
	}

	return
}

func (c *CategoryRepo) Update(ctx context.Context, req *catalog_service.UpdateCategory) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
			    "category"
			SET
				name = :name,
				parent_id = :parent_id,
				updated_at = now()
			WHERE
				id = :id`
	params = map[string]interface{}{
		"id":        req.GetId(),
		"name":      req.GetName(),
		"parent_id": helper.NewNullString(req.GetParentId()),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *CategoryRepo) UpdatePatch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error) {

	var (
		set   = " SET "
		ind   = 0
		query string
	)

	if len(req.Fields) == 0 {
		err = errors.New("no updates provided")
		return
	}

	req.Fields["id"] = req.Id

	for key := range req.Fields {
		set += fmt.Sprintf(" %s = :%s ", key, key)
		if ind != len(req.Fields)-1 {
			set += ", "
		}
		ind++
	}

	query = `
		UPDATE
			"category"
	` + set + ` , updated_at = now()
		WHERE
			id = :id
	`

	query, args := helper.ReplaceQueryParams(query, req.Fields)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), err
}

func (c *CategoryRepo) Delete(ctx context.Context, req *catalog_service.CategoryPrimaryKey) error {

	query := `UPDATE "category" SET deleted_at = now() WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return err
	}

	return nil
}
