package storage

import (
	"context"

	"catalog_service/genproto/catalog_service"
	"catalog_service/models"
)

type StorageI interface {
	CloseDB()
	Author() AuthorRepoI
	Book() BookRepoI
	BookCategory() BookCategoryRepoI
	Category() CategoryRepoI
}

type AuthorRepoI interface {
	Create(ctx context.Context, req *catalog_service.CreateAuthor) (resp *catalog_service.AuthorPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *catalog_service.AuthorPrimaryKey) (resp *catalog_service.Author, err error)
	GetAll(ctx context.Context, req *catalog_service.GetListAuthorRequest) (resp *catalog_service.GetListAuthorResponse, err error)
	Update(ctx context.Context, req *catalog_service.UpdateAuthor) (rowsAffected int64, err error)
	UpdatePatch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *catalog_service.AuthorPrimaryKey) error
}

type BookRepoI interface {
	Create(ctx context.Context, req *catalog_service.CreateBook) (resp *catalog_service.BookPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *catalog_service.BookPrimaryKey) (resp *catalog_service.Book, err error)
	GetAll(ctx context.Context, req *catalog_service.GetListBookRequest) (resp *catalog_service.GetListBookResponse, err error)
	Update(ctx context.Context, req *catalog_service.UpdateBook) (rowsAffected int64, err error)
	UpdatePatch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *catalog_service.BookPrimaryKey) error
}

type BookCategoryRepoI interface {
	Create(ctx context.Context, req *catalog_service.CreateBookCategory) (resp *catalog_service.BookCategoryPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *catalog_service.BookCategoryPrimaryKey) (resp *catalog_service.BookCategoryList, err error)
	GetAll(ctx context.Context, req *catalog_service.GetListBookCategoryRequest) (resp *catalog_service.GetListBookCategoryResponse, err error)
	Update(ctx context.Context, req *catalog_service.UpdateBookCategory) (rowsAffected int64, err error)
	UpdatePatch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *catalog_service.DeleteBookCategory) error
}

type CategoryRepoI interface {
	Create(ctx context.Context, req *catalog_service.CreateCategory) (resp *catalog_service.CategoryPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *catalog_service.CategoryPrimaryKey) (resp *catalog_service.CategoryList, err error)
	GetAll(ctx context.Context, req *catalog_service.GetListCategoryRequest) (resp *catalog_service.GetListCategoryResponse, err error)
	Update(ctx context.Context, req *catalog_service.UpdateCategory) (rowsAffected int64, err error)
	UpdatePatch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *catalog_service.CategoryPrimaryKey) error
}
