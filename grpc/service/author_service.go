package service

import (
	"context"

	"github.com/golang/protobuf/ptypes/empty"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"catalog_service/config"
	"catalog_service/genproto/catalog_service"
	"catalog_service/grpc/client"
	"catalog_service/models"
	"catalog_service/pkg/logger"
	"catalog_service/storage"
)

type AuthorService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*catalog_service.UnimplementedAuthorServiceServer
}

func NewAuthorService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *AuthorService {
	return &AuthorService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *AuthorService) Create(ctx context.Context, req *catalog_service.CreateAuthor) (resp *catalog_service.Author, err error) {

	i.log.Info("---CreateAuthor------>", logger.Any("req", req))

	pKey, err := i.strg.Author().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateAuthor->Author->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.Author().GetByPKey(ctx, pKey)
	if err != nil {
		i.log.Error("!!!GetByPKeyAuthor->Author->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *AuthorService) GetByID(ctx context.Context, req *catalog_service.AuthorPrimaryKey) (resp *catalog_service.Author, err error) {

	i.log.Info("---GetAuthorByID------>", logger.Any("req", req))

	resp, err = i.strg.Author().GetByPKey(ctx, req)
	if err != nil {
		i.log.Error("!!!GetAuthorByID->Author->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *AuthorService) GetList(ctx context.Context, req *catalog_service.GetListAuthorRequest) (resp *catalog_service.GetListAuthorResponse, err error) {

	i.log.Info("---GetAuthors------>", logger.Any("req", req))

	resp, err = i.strg.Author().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetAuthors->Author->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *AuthorService) Update(ctx context.Context, req *catalog_service.UpdateAuthor) (resp *catalog_service.Author, err error) {

	i.log.Info("---UpdateAuthor------>", logger.Any("req", req))

	rowsAffected, err := i.strg.Author().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateAuthor--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Author().GetByPKey(ctx, &catalog_service.AuthorPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetAuthor->Author->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *AuthorService) UpdatePatch(ctx context.Context, req *catalog_service.UpdatePatchAuthor) (resp *catalog_service.Author, err error) {

	i.log.Info("---UpdatePatchAuthor------>", logger.Any("req", req))

	updatePatchModel := models.UpdatePatchRequest{
		Id:     req.GetId(),
		Fields: req.GetFields().AsMap(),
	}

	rowsAffected, err := i.strg.Author().UpdatePatch(ctx, &updatePatchModel)

	if err != nil {
		i.log.Error("!!!UpdatePatchAuthor--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Author().GetByPKey(ctx, &catalog_service.AuthorPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetAuthor->Author->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *AuthorService) Delete(ctx context.Context, req *catalog_service.AuthorPrimaryKey) (resp *empty.Empty, err error) {

	i.log.Info("---DeleteAuthor------>", logger.Any("req", req))

	err = i.strg.Author().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteAuthor->Author->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &empty.Empty{}, nil
}
