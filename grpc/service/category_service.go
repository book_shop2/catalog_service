package service

import (
	"context"

	"github.com/golang/protobuf/ptypes/empty"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"catalog_service/config"
	"catalog_service/genproto/catalog_service"
	"catalog_service/grpc/client"
	"catalog_service/models"
	"catalog_service/pkg/logger"
	"catalog_service/storage"
)

type CategoryService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*catalog_service.UnimplementedCategoryServiceServer
}

func NewCategoryService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *CategoryService {
	return &CategoryService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *CategoryService) Create(ctx context.Context, req *catalog_service.CreateCategory) (resp *catalog_service.CategoryList, err error) {

	i.log.Info("---CreateCategory------>", logger.Any("req", req))

	pKey, err := i.strg.Category().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateCategory->Category->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.Category().GetByPKey(ctx, pKey)
	if err != nil {
		i.log.Error("!!!GetByPKeyCategory->Category->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *CategoryService) GetByID(ctx context.Context, req *catalog_service.CategoryPrimaryKey) (resp *catalog_service.CategoryList, err error) {

	i.log.Info("---GetCategoryByID------>", logger.Any("req", req))

	resp, err = i.strg.Category().GetByPKey(ctx, req)
	if err != nil {
		i.log.Error("!!!GetCategoryByID->Category->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *CategoryService) GetList(ctx context.Context, req *catalog_service.GetListCategoryRequest) (resp *catalog_service.GetListCategoryResponse, err error) {

	i.log.Info("---GetCategorys------>", logger.Any("req", req))

	resp, err = i.strg.Category().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetCategorys->Category->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *CategoryService) Update(ctx context.Context, req *catalog_service.UpdateCategory) (resp *catalog_service.CategoryList, err error) {

	i.log.Info("---UpdateCategory------>", logger.Any("req", req))

	rowsAffected, err := i.strg.Category().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateCategory--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Category().GetByPKey(ctx, &catalog_service.CategoryPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetCategory->Category->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *CategoryService) UpdatePatch(ctx context.Context, req *catalog_service.UpdatePatchCategory) (resp *catalog_service.CategoryList, err error) {

	i.log.Info("---UpdatePatchCategory------>", logger.Any("req", req))

	updatePatchModel := models.UpdatePatchRequest{
		Id:     req.GetId(),
		Fields: req.GetFields().AsMap(),
	}

	rowsAffected, err := i.strg.Category().UpdatePatch(ctx, &updatePatchModel)

	if err != nil {
		i.log.Error("!!!UpdatePatchCategory--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Category().GetByPKey(ctx, &catalog_service.CategoryPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetCategory->Category->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *CategoryService) Delete(ctx context.Context, req *catalog_service.CategoryPrimaryKey) (resp *empty.Empty, err error) {

	i.log.Info("---DeleteCategory------>", logger.Any("req", req))

	err = i.strg.Category().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteCategory->Category->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &empty.Empty{}, nil
}
