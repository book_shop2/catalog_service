package service

import (
	"context"

	"github.com/golang/protobuf/ptypes/empty"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"catalog_service/config"
	"catalog_service/genproto/catalog_service"
	"catalog_service/grpc/client"
	"catalog_service/models"
	"catalog_service/pkg/logger"
	"catalog_service/storage"
)

type BookCategoryService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*catalog_service.UnimplementedBookCategoryServiceServer
}

func NewBookCategoryService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *BookCategoryService {
	return &BookCategoryService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *BookCategoryService) Create(ctx context.Context, req *catalog_service.CreateBookCategory) (resp *catalog_service.BookCategoryList, err error) {

	i.log.Info("---CreateBookCategory------>", logger.Any("req", req))

	pKey, err := i.strg.BookCategory().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateBookCategory->BookCategory->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.BookCategory().GetByPKey(ctx, pKey)
	if err != nil {
		i.log.Error("!!!GetByPKeyBookCategory->BookCategory->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *BookCategoryService) GetByID(ctx context.Context, req *catalog_service.BookCategoryPrimaryKey) (resp *catalog_service.BookCategoryList, err error) {

	i.log.Info("---GetBookCategoryByID------>", logger.Any("req", req))

	resp, err = i.strg.BookCategory().GetByPKey(ctx, req)
	if err != nil {
		i.log.Error("!!!GetBookCategoryByID->BookCategory->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *BookCategoryService) GetList(ctx context.Context, req *catalog_service.GetListBookCategoryRequest) (resp *catalog_service.GetListBookCategoryResponse, err error) {

	i.log.Info("---GetBookCategorys------>", logger.Any("req", req))

	resp, err = i.strg.BookCategory().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetBookCategorys->BookCategory->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *BookCategoryService) Update(ctx context.Context, req *catalog_service.UpdateBookCategory) (resp *catalog_service.BookCategoryList, err error) {

	i.log.Info("---UpdateBookCategory------>", logger.Any("req", req))

	rowsAffected, err := i.strg.BookCategory().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateBookCategory--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.BookCategory().GetByPKey(ctx, &catalog_service.BookCategoryPrimaryKey{BookId: req.BookId})
	if err != nil {
		i.log.Error("!!!GetBookCategory->BookCategory->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *BookCategoryService) UpdatePatch(ctx context.Context, req *catalog_service.UpdatePatchBookCategory) (resp *catalog_service.BookCategoryList, err error) {

	i.log.Info("---UpdatePatchBookCategory------>", logger.Any("req", req))

	updatePatchModel := models.UpdatePatchRequest{
		Id:     req.GetBookId(),
		Fields: req.GetFields().AsMap(),
	}

	rowsAffected, err := i.strg.BookCategory().UpdatePatch(ctx, &updatePatchModel)

	if err != nil {
		i.log.Error("!!!UpdatePatchBookCategory--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.BookCategory().GetByPKey(ctx, &catalog_service.BookCategoryPrimaryKey{BookId: req.BookId})
	if err != nil {
		i.log.Error("!!!GetBookCategory->BookCategory->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *BookCategoryService) Delete(ctx context.Context, req *catalog_service.DeleteBookCategory) (resp *empty.Empty, err error) {

	i.log.Info("---DeleteBookCategory------>", logger.Any("req", req))

	err = i.strg.BookCategory().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteBookCategory->BookCategory->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &empty.Empty{}, nil
}
