package grpc

import (
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"

	"catalog_service/config"
	"catalog_service/genproto/catalog_service"
	"catalog_service/grpc/client"
	"catalog_service/grpc/service"
	"catalog_service/pkg/logger"
	"catalog_service/storage"
)

func SetUpServer(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) (grpcServer *grpc.Server) {

	grpcServer = grpc.NewServer()

	catalog_service.RegisterAuthorServiceServer(grpcServer, service.NewAuthorService(cfg, log, strg, srvc))
	catalog_service.RegisterBookCategoryServiceServer(grpcServer, service.NewBookCategoryService(cfg, log, strg, srvc))
	catalog_service.RegisterBookServiceServer(grpcServer, service.NewBookService(cfg, log, strg, srvc))
	catalog_service.RegisterCategoryServiceServer(grpcServer, service.NewCategoryService(cfg, log, strg, srvc))

	reflection.Register(grpcServer)
	return
}
